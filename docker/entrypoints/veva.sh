#!/bin/bash

: "${VEVA_ROOT:=/veva/app}"
echo ${PWD} > /tmp/.workdir

TMP_FILE=$(mktemp)
cat << EOH > ${TMP_FILE}
cd ${VEVA_ROOT}
exec pipenv run $@
EOH
chmod +x ${TMP_FILE}

exec ${VEVA_ROOT}/docker/scripts/handle-guids.sh --user veva --script-path ${TMP_FILE}
