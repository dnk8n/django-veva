#!/bin/bash

: ${HOST_UID:=1000}
: ${HOST_GID:=1000}

while [[ "$#" > 0 ]]; do case $1 in
  -u|--user) CONTAINER_USERNAME="$2"; shift;;
  -s|--script-path) SCRIPT_PATH="$2"; shift;;
  *) echo "Unknown parameter passed: $1"; exit 1;;
esac; shift; done

IFS='' read -r -d '' SCRIPT_HEADER <<"EOF"
#!/bin/bash
echo "       The remainder of entrypoint is run as user, $(whoami)"
export LC_ALL=C.UTF-8
export LANG=C.UTF-8
EOF
echo "${SCRIPT_HEADER}
$(cat ${SCRIPT_PATH})" > ${SCRIPT_PATH}

if [[ $(id -u) -eq 0 ]]; then
    if [[ (${HOST_UID} -ne 1000 || ${HOST_GID} -ne 1000) && $(id -u ${CONTAINER_USERNAME}) -eq 1000 && $(id -g ${CONTAINER_USERNAME}) -eq 1000 ]]; then
        echo "[INFO] Container run as $(whoami) implies that a change of ${CONTAINER_USERNAME} user's uid & gid is required and is now being performed..."
        usermod -u ${HOST_UID} ${CONTAINER_USERNAME}
        groupmod -g ${HOST_GID} ${CONTAINER_USERNAME}
        chown -R ${HOST_UID}:${HOST_GID} /${CONTAINER_USERNAME}
    else
        echo "[INFO] Container run as $(whoami), however user and group mods have already been performed"
    fi
else
    echo "[WARN] Container run as $(whoami) implies that no user or group mods will take place"
    echo "       The following variables are therefore being ignored: 'HOST_UID=${HOST_UID}' and 'HOST_GID=${HOST_GID}'"
    echo "       If you expected user modifications to be performed, please rerun with variable 'CONTAINER_USER=root' set"
    exec "${SCRIPT_PATH}"
fi

chown ${CONTAINER_USERNAME}:${CONTAINER_USERNAME} ${SCRIPT_PATH}
exec su -s "/bin/bash" -c "${SCRIPT_PATH}" - ${CONTAINER_USERNAME}
