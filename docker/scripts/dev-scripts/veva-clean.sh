#!/bin/bash

# Run this on the host if you want a clean start. This will remove all
# containers and docker volumes of the compose directory specified
# [WARNING] Only intended for a dev environment, do not run on production

while [[ "$#" > 0 ]]; do case $1 in
  -d|--dir-to-project) PROJECT_DIR="$2"; shift;;
  *) echo "Unknown parameter passed: $1"; exit 1;;
esac; shift; done

# Assume dev unless prod flag is passed
THIS_SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
cd ${PROJECT_DIR:-${THIS_SCRIPT_DIR}/../../..}
docker-compose down -v --remove-orphans
